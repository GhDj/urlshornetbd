<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'ShortUrlController@index')->name('home');

Route::get('/language/{lang}', 'LanguageController@switchLang')->name('changeLang');

Route::get('/shorturl','ShortUrlController@index')->name('shorturl.index');

Route::get('/shorturl/add','ShortUrlController@create')->name('shorturl.create')->middleware('auth');
Route::get('/shorturl/{id}','ShortUrlController@show')->name('shorturl.show')->middleware('auth');
Route::post('/shorturl/add','ShortUrlController@store')->name('shorturl.store')->middleware('auth');
Route::delete('/shorturl/{id}','ShortUrlController@destroy')->name('shorturl.delete')->middleware('auth');

Route::get('/{code}','ShortUrlController@redirecting')->name('shorturl.redirect');
