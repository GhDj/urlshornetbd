<?php

return [

    'failed' => 'Ces informations d\'identification ne correspondent pas.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',

];

