@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $link->id }} : {{ Config::get('app.url').'/'.$link->short_url }} <a class="btn btn-success btn-sm float-end" href="{{ route('shorturl.create') }}">{{ __('language.add') }}</a></div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-warning" role="alert">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        @endif

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('language.base_url') }}</th>
                                <th scope="col">{{ __('language.short_url') }}</th>
                                <th scope="col">{{ __('language.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <th scope="row">{{ $link->id }}</th>
                                    <td><a href="{{ $link->base_url }}">{{ $link->base_url }}</a></td>
                                    <td><a href="{{ Config::get('app.url').'/'.$link->short_url }}">{{ Config::get('app.url').'/'.$link->short_url }}</a></td>
                                    <td>
                                        <form method="POST" action="{{ route('shorturl.delete', $link->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-outline-danger btn-sm" type="submit">{{ __('language.delete') }}</button>
                                        </form>
                                        <a class="btn btn-sm btn-outline-info" href="{{ route('shorturl.show', $link->id) }}">{{ __('language.details') }}</a>

                                    </td></tr>

                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('language.user_id') }}</th>
                                <th scope="col">{{ __('language.ip') }}</th>
                                <th scope="col">{{ __('language.location') }}</th>
                                <th scope="col">{{ __('language.user_agent') }}</th>
                                <th scope="col">{{ __('language.date_time') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($logs as $log)
                                <tr>
                                    <th scope="row">{{ $loop->index+1 }}</th>
                                    <td>{{ $log->user_id }}</td>
                                    <td>{{ $log->ip }}</td>
                                    <td>{{ $log->location }}</td>
                                    <td>{{ $log->user_agent }}</td>
                                    <td>{{ $log->created_at }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">{{ __('language.empty') }}</td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
