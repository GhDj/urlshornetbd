@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('language.add_url') }}</div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-warning" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </div>
                            @endif
                            <form class="row align-items-center" method="post" action="{{ route('shorturl.store') }}">
                                @csrf
                                <div class="col-10">
                                    <label class="visually-hidden" for="inlineFormInputGroupUsername">{{ __('language.base_url') }}</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="inlineFormInputGroupUsername" name="url" placeholder="{{ __('language.base_url') }}">
                                    </div>
                                </div>




                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary">{{ __('language.add') }}</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
