<?php

namespace App\Console\Commands;

use App\ShortUrl;
use Illuminate\Console\Command;

class DeleteExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remover url after 24 hours of creation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ShortUrl::where('created_at','<=',  now()->addHours("24"))->delete();
    }
}
