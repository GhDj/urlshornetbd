<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortUrl extends Model
{
    protected $fillable = [
        'base_url', 'short_url', 'user_id'
    ];

    public function user() {
        return $this->belongsTo('User');
    }

    public function getlogs() {
        return $this->hasMany(Log::class);
    }
}
