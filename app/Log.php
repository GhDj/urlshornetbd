<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'short_url_id', 'user_id', 'ip', 'location', 'user_agent'
    ];

    public function url() {
        $this->belongsTo(ShortUrl::class);
    }
}
