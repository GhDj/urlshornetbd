<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreShortUrlRequest;
use App\Log;
use App\ShortUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Stevebauman\Location\Facades\Location;

class ShortUrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ((Auth::user() && (Auth::user()->shorturls()->count() > 0)) ){
            $links = Auth::user()->shorturls()->get();
          //  dd($links);
            return view('shorturl.index', ["links" => $links]);
        } else {
            return redirect(route('shorturl.create'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->shorturls()->count() >= 5) {
            $links = Auth::user()->shorturls()->get();
            return view('shorturl.index',["links" => $links])->withErrors(__('language.max_reached'));
        } else {
            return view('shorturl.create');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShortUrlRequest $request)
    {
        $base_url = $request->get('url');
        $code = Str::random(6);

        if (ShortUrl::all()->count() >= 20) {
            $oldest = ShortUrl::oldest()->first();
            $oldest->delete();
            ShortUrl::create([
                'base_url' => $base_url,
                'short_url' => $code,
                'user_id' => Auth::id()
            ]);
        } else {
            ShortUrl::create([
                'base_url' => $base_url,
                'short_url' => $code,
                'user_id' => Auth::id()
            ]);
        }

        return back()->with('success',__('language.link_added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $link = ShortUrl::find($id);
        $logs = $link->getlogs()->get();
        return view('shorturl.show')->with(['link' => $link, 'logs' => $logs]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $short_url = ShortUrl::find($id);
        if (Auth::id() == $short_url->user_id) {
            $short_url->delete();
            return redirect(route('shorturl.index'))->with('success', __('language.deleted'));
        } else {
            return abort(403);
        }
    }

    public function redirecting($code) {
        $link = ShortUrl::where('short_url', $code)->first();
        if (Auth::user()) {
            $uid = Auth::id();
        } else {
            $uid = null;
        }
        if (Location::get(\request()->ip()))
        {
            $location = Location::get(\request()->ip())->countryName;
        } else {
            $location = __('language.undefined');
        }
        Log::create([
            'user_id' => $uid,
            'ip' => \request()->ip(),
            'location' => $location,
            'user_agent' => \request()->userAgent(),
            'short_url_id' => $link->id
        ]);
        return redirect($link->base_url);
    }
}
